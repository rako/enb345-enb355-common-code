﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace ENB354_ENB355_GCS_COMMON
{
    public class Command : INotifyPropertyChanged
    {
        public enum CommandSource
        {
            GCS = 0x00,
            Blimp = 0x01
        }

        
        private string _source;
        private string _time;
        private string _commandMessage;
        public string Source 
        {
            get { return _source; }            
        }
        public string Time 
        {
            get { return _time; }
        }
        public string CommandMessage
        {
            get { return _commandMessage; }
        }

        public Command(string source, string time, string commandMessage)
        {
            _source = source;
            _time = time;
            _commandMessage = commandMessage;
        }

        public Command(byte[] command, ref int pos)
        {
            CommandSource source = (CommandSource)command[pos];
            pos+=1;
            string entireMessage = Helper.getStringFromBytes(command, ref pos);
            if (entireMessage.Length <= 8)
            {
                throw new Exception("command string too short: " + entireMessage);
            }
            _time = entireMessage.Substring(0,8);

            _commandMessage = entireMessage.Substring(8);

            _source = source.ToString();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
    }
}
