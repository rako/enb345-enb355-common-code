﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ENB354_ENB355_GCS_COMMON
{
    

    public enum PacktHeader
    {
        LoginRequest = 0x00,
        ReplyLoginRequest = 0x01,
        SensorDataStructure = 0x02,
        SensorData = 0x03,
        Commands = 0x04,
        CommandBlimp = 0x40,
        Deliver = 0x41,
        Survivor = 0x42,

        Control = 0x43,
        DeliverTo = 0x44
        //todo add to documentation survivor packet
    }

    public enum CommandValue
    {
        TakeOff = 0x00,
        Circumnavigate = 0x01,
        Search = 0x02,
        Land = 0x03
        //add land to documentation
    }

    public enum ErrorCodes
    {
        NoError = 0,
        Invalid = 1,
        UnknownTypeOfPacket = 2,
        DataPacketSizeMismatch = 3,
        AdminLogin = 44,
    }


    public class Networking
    {
        public const int HeaderSize = 5;
    }
}
