﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ENB354_ENB355_GCS_COMMON
{
    public class CommandLog
    {
        

        //store commands as their bytes ready for sending
        private LinkedList<byte[]> _commands;

        public CommandLog()
        {
            _commands = new LinkedList<byte[]>();
        }

        public void add(Command.CommandSource source, string commandMessage)
        {
            //add time
            commandMessage = DateTime.Now.ToString("HH:mm:ss") + commandMessage;

            //create byte array
            byte[] byteArray = new byte[commandMessage.Length + 2];

            //populate byte array
            byteArray[0] = (byte)source;
            Helper.StringToByteArray(commandMessage).CopyTo(byteArray, 1);

            //add to internal storage
            _commands.AddLast(byteArray);
        }

        public byte[] toBytes(ref LinkedListNode<byte[]> lastCommand)
        {
            //check if no more or if not yet started
            if (lastCommand == null)
            {
                lastCommand = _commands.First;
            }
            else if (lastCommand.Next != null)
            {
                lastCommand = lastCommand.Next;
            }
            else
            {
                return new byte[0];
            }

            LinkedListNode<byte[]> tempLastCommand = lastCommand;
            int numBytes = 0;
            UInt16 numCommandas = 0;
            bool loop = true;
            do
            {
                numBytes += lastCommand.Value.Length;
                numCommandas++;
                if (lastCommand.Next != null)
                {
                    lastCommand = lastCommand.Next;
                }
                else
                {
                    loop = false;
                }
            }
            while (loop);

            byte[] byteArray = new byte[numBytes + 2];
            int pos = 0;
            BitConverter.GetBytes(numCommandas).CopyTo(byteArray, 0);
            pos += 2;

            for (int i = 0; i < numCommandas; ++i)
            {
                tempLastCommand.Value.CopyTo(byteArray, pos);
                pos += tempLastCommand.Value.Length;
                tempLastCommand = tempLastCommand.Next;
            }

            return byteArray;
        }
    }
}
