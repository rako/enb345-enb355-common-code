﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace ENB354_ENB355_GCS_COMMON
{
    public class Helper
    {
        public static byte[] StringToByteArray(string str)
        {
            byte[] byteArray = null;
            byte lengthStr = (byte)str.Length;
            byteArray = new byte[lengthStr + 1];
            byteArray[0] = lengthStr;
            Encoding.ASCII.GetBytes(str).CopyTo(byteArray, 1);
            return byteArray;
        }

        public static string getStringFromBytes(byte[] byteArray, ref int offset)
        {
            if (offset >= byteArray.Length)
                throw new Exception("data not big enough to get string");
            byte size = byteArray[offset];
            
            if (offset + size >= byteArray.Length)
                throw new Exception("data not big enough to get string");
            string outString = Encoding.ASCII.GetString(byteArray, offset + 1, size);
            offset += 1 + size;
            return outString;
        }

        public static byte[] read(NetworkStream clientStream, int size)
        {
            byte[] red = new byte[size];
            int redNumTot = 0;
            int redNum = 0;
            do
            {
                redNum = clientStream.Read(red, redNumTot, size - redNumTot);
                redNumTot += redNum;
            } while (redNumTot < size && redNum != 0);

            if (redNum == 0)
            {
                //TODO throw better exception
                throw new System.ArgumentException("empty packet recieved Ended");
            }

            return red;
        }

        public static byte[] read(NetworkStream clientStream, int size, ref DateTime lastUpdate) {
            byte[] red = new byte[size];
            int redNumTot = 0;
            int redNum = 0;
            do {
                redNum = clientStream.Read(red, redNumTot, size - redNumTot);
                lastUpdate = DateTime.Now;
                redNumTot += redNum;
            } while (redNumTot < size && redNum != 0);

            if (redNum == 0) {
                //TODO throw better exception
                throw new System.ArgumentException("empty packet recieved Ended");
            }

            return red;
        }

        public static string getLocalIP()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }
    }
}
