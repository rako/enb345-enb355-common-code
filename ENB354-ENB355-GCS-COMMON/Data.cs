﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Diagnostics;
using System.ComponentModel;

namespace ENB354_ENB355_GCS_COMMON
{
    public class Data : INotifyPropertyChanged
    {
        #region notifiy change stuff
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(string name)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }
        #endregion

        #region data type
        public enum DataType
        {
            UC8 = 0x00,
            SI32 = 0x01,
            FLOAT = 0x02,
            STRING = 0x88,
            JPEG = 0xC0
        }
        #endregion

        #region veriables and properties
        private DataType _dataType;
        public object _value;
        public object _min;
        public object _max;
        public string _name;
        public bool _hidden = false;

        public bool hidden
        {
            get { return _hidden; }
            //set { value = _hidden; }
        }

        public DataType dataType
        {
            get { return _dataType; }
        }

        public object value
        {
            get { return _value; }
        }

        public object min
        {
            get { return _min; }
        }

        public object max
        {
            get { return _max; }
        }

        public string name
        {
            get { return _name; }
        }
        

        private Object thisLock = new Object();
        #endregion

        #region server constructors
        public Data(string name, object value)
        {
            this._value = value;
            _min = null;
            _max = null;
            DetermineDataType(value);
            _name = name;
        }

        public Data(string name, int value, int min, int max)
        {
            _value = value;
            _min = min;
            _max = max;
            _dataType = DataType.SI32;
            _name = name;
        }

        public Data(string name, float value, float min, float max)
        {
            _value = value;
            _min = min;
            _max = max;
            _dataType = DataType.SI32;
            _name = name;
        }

        public Data(string name, Image value)
        {
            _value = value;
            _min = min;
            _max = max;
            _dataType = DataType.JPEG;
            _name = name;
        }
        #endregion

        #region client Constructors
        public Data(DataType dataType, String dataName, bool hidden = false )
        {
            _dataType = dataType;
            _name = dataName;
            _hidden = hidden;
        }

        public Data(float min, float max, String dataName, bool hidden = false)
        {
            _dataType = DataType.FLOAT;
            _min = min;
            _max = max;
            _name = dataName;
            _hidden = hidden;
        }

        public Data(int min, int max, String dataName, bool hidden = false)
        {
            _dataType = DataType.SI32;
            _min = min;
            _max = max;
            _name = dataName;
            _hidden = hidden;
        }
        #endregion

        #region data to bytes
        public byte[] toBytes()
        {
            lock (thisLock)
            {
                byte[] toSend = null;
                switch (dataType)
                {
                    case DataType.UC8:
                        toSend = new byte[] {(byte)value};
                        break;
                    case DataType.SI32:
                        toSend = BitConverter.GetBytes((Int32)value);
                        break;
                    case DataType.FLOAT:
                        toSend = BitConverter.GetBytes((Single)value);
                        break;
                    case DataType.STRING:
                        byte[] stringByte = Helper.StringToByteArray((string)value);
                        UInt32 lengthString = (UInt32)stringByte.Length;
                        byte[] fullStringByte = new byte[lengthString + 4];
                        stringByte.CopyTo(fullStringByte, 4);
                        BitConverter.GetBytes(lengthString).CopyTo(toSend, 0);
                        toSend = fullStringByte;
                        break;
                    case DataType.JPEG:
                        
                            Image jpeg = (Image)value;
                            MemoryStream ms = new MemoryStream();
                            // Save to memory using the Jpeg format
                            jpeg.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            UInt32 lengthJpeg = (UInt32)ms.Length;
                            toSend = new byte[lengthJpeg + 4];
                            BitConverter.GetBytes(lengthJpeg).CopyTo(toSend, 0);
                            ms.ToArray().CopyTo(toSend, 4);
                            ms.Close();
                        
                        break;
                    default:
                        Debug.Assert(false, "Data does not have a correct data type");
                        throw new Exception("Fatal Exception: data type still unknown");
                }
                return toSend;
            }
        }
        



        public byte[] MinToBytes()
        {
            switch (dataType)
            {
                case DataType.SI32:
                    return BitConverter.GetBytes((Int32)value);
                case DataType.FLOAT:
                    return BitConverter.GetBytes((Single)value);
                case DataType.UC8:
                case DataType.STRING:
                case DataType.JPEG:
                    return new byte[0];
                default:
                    Debug.Assert(false, "Data does not have a correct data type");
                    throw new Exception("Fatal Exception: data type still unknown");
            }
        }

        public byte[] MaxToBytes()
        {
            switch (dataType)
            {
                case DataType.SI32:
                    return BitConverter.GetBytes((Int32)value);
                case DataType.FLOAT:
                    return BitConverter.GetBytes((Single)value);
                case DataType.UC8:
                case DataType.STRING:
                case DataType.JPEG:
                    return new byte[0];
                default:
                    Debug.Assert(false, "Data does not have a correct data type");
                    throw new Exception("Fatal Exception: data type still unknown");
            }
        }
        #endregion

        #region update
        public void Update(string value)
        {
            lock (thisLock)
            {
                Check(value);
                Debug.Assert(((string)value).Length <= 0xff, "String is too long");
                if (((string)value).Length > 0xff)
                {
                    throw new Exception("String Too Long");
                }
                _value = value;
                this.NotifyPropertyChanged("value");
            }
        }

        public void Update(object value)
        {
            lock (thisLock)
            {
                Check(value);
                _value = value;
                this.NotifyPropertyChanged("value");

            }
        }

        public void Update(byte[] data, ref int pos)
        {
            lock (thisLock)
            {
                switch (dataType)
                {
                    case DataType.SI32:
                        _value = BitConverter.ToInt32(data, pos);
                        if ((Int32)_value <= (Int32)min) {
                            _value = "<= " + min.ToString();
                        }
                        else if ((Int32)_value >= (Int32)max) {
                            _value = ">= " + max.ToString();
                        }
                        pos += 4;
                        break;
                    case DataType.FLOAT:
                        _value = BitConverter.ToSingle(data, pos);
                        if ((Single)_value <= (Single)min) {
                            _value = "<= " + min.ToString();
                        } else if ((Single)_value >= (Single)max) {
                            _value = ">= " + max.ToString();
                        }
                        pos += 4;
                        break;
                    case DataType.UC8:
                        _value = data[pos];
                        pos += 1;
                        break;
                    case DataType.STRING:
                        UInt32 sizeString = BitConverter.ToUInt32(data, pos);
                        pos += 4;
                        _value = Helper.getStringFromBytes(data, ref pos);
                        break;
                    case DataType.JPEG:
                        UInt32 size = BitConverter.ToUInt32(data, pos);
                        pos += 4;
                        if (size > 0)
                        {
                            MemoryStream ms = new MemoryStream(data, pos, (int)size);
                            _value = Image.FromStream(ms);
                            pos += (int)size;
                        }
                        break;
                    default:
                        Debug.Assert(false, "Data does not have a correct data type");
                        throw new Exception("Fatal Exception: data type still unknown");
                }
                this.NotifyPropertyChanged("value");

            }
        }
        #endregion

        #region check data type
        private void Check(object value)
        {
            if (!value.GetType().IsEquivalentTo(this.value.GetType()))
                throw new Exception("new type is not equivilent to current type");
        }
        #endregion

        #region determine data type
        private void DetermineDataType(object value)
        {
            switch (Type.GetTypeCode(value.GetType()))
            {
                case TypeCode.Byte:
                    _dataType = DataType.UC8;
                    break;
                case TypeCode.Int32:
                    _dataType = DataType.SI32;
                    _min = Int32.MinValue;
                    _max = Int32.MaxValue;
                    break;
                case TypeCode.Single:
                    _dataType = DataType.FLOAT;
                    _min = Single.MinValue;
                    _max = Single.MaxValue;
                    break;
                case TypeCode.String:
                    Debug.Assert(((string)value).Length <= 0xff, "String is too long");
                    Update(value);
                    _dataType = DataType.STRING;
                    break;
                default:
                    throw new Exception("Error data type not known");
            }
        }
        #endregion
    }
}
